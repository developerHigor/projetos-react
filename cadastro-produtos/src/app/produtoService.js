export const PRODUTOS = '_PRODUTOS';

export function ErroValidacao (errors){
    this.errors = errors;
}

export default class ProdutoService {
    validar = (produto) => {
        let errors = [];

        if(!produto.nome)
            errors.push("O campo Nome é obrigatório.");

        if(!produto.sku)
            errors.push("O campo SKU é obrigatório.");

        if(!produto.fornecedor)
            errors.push("O campo Fornecedor é obrigatório.");

        if(!produto.preco || produto.preco <= 0)
            errors.push("O campo Preço deve ter um valor válido.");

        if(errors.length > 0){
            throw new ErroValidacao(errors);
        }
    }

    getProdutos = () => {
        return JSON.parse(localStorage.getItem(PRODUTOS));
    }

    getProdutosBySku = (sku) => {
        const produtos = JSON.parse(localStorage.getItem(PRODUTOS));
        return produtos?.filter(produto => produto.sku === sku)[0];
    }

    getIndex = (sku) => {
        let produtos = this.getProdutos();
        const item = produtos.find(p => p.sku === sku);
        return produtos.indexOf(item);
    }

    salvar = (produto) => {
        this.validar(produto);
        let produtos = this.getProdutos();

        if(!produtos || produtos.length === 0){
            produtos = [];
            produtos.push(produto);
        }
        else{
            const index = this.getIndex(produto.sku);
            if(index >= 0){ 
                produtos[index] = produto;
            }
            else
                produtos.push( produto);
        }

        localStorage.setItem(PRODUTOS, JSON.stringify(produtos));

        return true;
    }

    deletar = (sku) => {
        const produtos = this.getProdutos();
        const index = this.getIndex(sku);
        produtos.splice(index, 1);
        localStorage.setItem(PRODUTOS, JSON.stringify(produtos));
        return produtos;
    }
}
