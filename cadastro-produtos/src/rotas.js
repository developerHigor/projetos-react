import React from "react";

import { Routes, Route } from 'react-router-dom';

import Home from "./View/home";
import CadastroProduto from "./View/produtos/cadastro";
import ConsultaProdutos from "./View/produtos/consulta";

const Rotas = () => {
    return (
        <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/cadastro-produtos" element={<CadastroProduto />} />
            <Route exact path="/cadastro-produtos/:sku" element={<CadastroProduto />} />
            
            <Route exact path="/consulta-produtos" element={<ConsultaProdutos />} />
        </Routes>
    )
}

export default Rotas;
