export const onChangeStateComponente = (obj, event) => {
    const campo = event.target.id;
    const valor = event.target.value;
    obj.setState({ [campo] : valor });
}

export const onChangeState = (obj, handler, event) => {
    const campo = event.target.id;
    const valor = event.target.value;
    const newObj = {...obj, ...{ [campo] : valor }};
    
    handler(newObj);
}
