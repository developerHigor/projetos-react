import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from 'react-router-dom';
import { onChangeState } from "../../Utils";
import ProdutoService from "../../app/produtoService";
import Produto from "../../Model/Produto";
import Card from "../../components/Card";

const estadoInicialProduto = {
    nome: '',
    sku: '',
    descricao: '',
    fornecedor: '',
    preco: 0
};

const estadoInicialTela = {
    errors: [],
    sucessoSalvar: false,
    showAlertDanger: true,
    showAlertSuccess: true
}

export default function CadastroProduto(){
    const service = new ProdutoService();
    const navigate = useNavigate();
    const params = useParams();
    const [state, setState] = useState(estadoInicialTela);
    const [produto, setProduto] = useState(estadoInicialProduto);
    const [edicao, setEdicao] = useState(false);

    useEffect(() => {
        const { sku } = params;
        if(sku){
            setEdicao(true);
            const objProduto = service.getProdutosBySku(sku)
            if(objProduto !== undefined)
                setProduto({...objProduto});
            else{
                const error = { errors: [ 'Produto não encontrado' ] };
                setState({...state, ...{ showAlertDanger: true, errors : error.errors }});
            }
        }
    }, []);

    function onSubmite(){
        const objProduto = new Produto({
            nome: produto.nome,
            sku: produto.sku,
            descricao: produto.descricao,
            fornecedor: produto.fornecedor,
            preco: produto.preco
        })

        try {
            service.salvar(objProduto);
            onClear();
            setState({...state, ...{ sucessoSalvar : true, showAlertSuccess: true }});
        } catch (error) {
            setState({...state, ...{ showAlertDanger: true, errors : error.errors }});
        }
    }
    

    function onClear() {
        setProduto(estadoInicialProduto);
    };

    function handleDismissDanger(){
        setState({...state, ...{ showAlertDanger: false  }});
    }

    function handleDismissSuccess(){
        setState({...state, ...{ showAlertSuccess: false  }});
    }
    
    return (
        <Card header={<h2><span onClick={() => navigate(-1)} className="material-icons-outlined" style={{ paddingRight: '10px', paddingTop: '10px', cursor: 'pointer'}}> keyboard_backspace </span>{ !edicao ? 'Cadastro de produtos' : 'Atualização de Produto' }</h2>}>
            {
                state.sucessoSalvar && state.showAlertSuccess &&
                (
                    <div className="alert alert-dismissible alert-success">
                        <button type="button" className="btn-close" data-bs-dismiss="alert" onClick={handleDismissSuccess}></button>
                        <strong>Sucesso</strong> Deu tudo certo no salvar o produto.
                    </div>
                ) 
            }

            {
                state.errors?.length > 0 && state.showAlertDanger &&
                (
                    <div className="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" className="btn-close" data-dismiss="alert" aria-label="Close" onClick={handleDismissDanger}></button>
                        <strong>Erro</strong>
                            {
                                state.errors.map(erro => {
                                    return <div key={erro}>{erro}</div>
                                })
                            }
                    </div>
                ) 
            }

            <div className="row">
                <div className="col-md-6">
                    <div className="form-group">
                        <label>Nome: *</label>
                        <input type="text" className="form-control" id="nome" value={produto.nome}
                                onChange={e => onChangeState(produto, setProduto, e)} 
                        />
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="form-group">
                        <label>SKU: *</label>
                        <input type="text" className="form-control" id="sku" value={produto.sku}
                                onChange={e => onChangeState(produto, setProduto, e)} disabled={ edicao }
                        />
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-12">
                    <div className="form-group">
                        <label>Descricao: </label>
                        <textarea className="form-control" id="descricao" value={produto.descricao}
                                    onChange={e => onChangeState(produto, setProduto, e)} 
                        />
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-6">
                    <div className="form-group">
                        <label>Fornecedor: *</label>
                        <input type="text" className="form-control" id="fornecedor" value={produto.fornecedor}
                                onChange={e => onChangeState(produto, setProduto, e)} 
                        />
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="form-group">
                        <label>Preço: *</label>
                        <input type="number" className="form-control" id="preco" value={produto.preco}
                                onChange={e => onChangeState(produto, setProduto, e)} 
                        />
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-3 col-md-1 col-lg-1">
                    <button className="btn btn-success" onClick={() => onSubmite()}>{ !edicao ? 'Salvar' : 'Atualizar' } </button>
                </div>
                <div className="col-3 col-md-1 col-lg-1" style={{marginLeft: '20px'}}>
                    <button className="btn btn-primary" onClick={() => onClear()}>Limpar</button>
                </div>
            </div>
        </Card>
    )
}
