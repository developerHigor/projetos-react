import React from "react";

const ProdutosTable = (props) => (
    <table id="datatable" className="table table-striped table-no-bordered table-hover">
        <thead>
            <tr>
            <th scope="col">Nome</th>
            <th scope="col">SKU</th>
            <th scope="col">Fornecedor</th>
            <th scope="col">Preço</th>
            <th></th>
            </tr>
        </thead>
        <tbody>
            {
                props?.produtos?.map(produto => {
                    return (
                        <tr key={produto.sku}>
                            <td>{produto.nome}</td>
                            <td>{produto.sku}</td>
                            <td>{produto.fornecedor}</td>
                            <td>{produto.preco}</td>
                            <td>
                                <button className="btn btn-primary" onClick={ () => props.editar(produto.sku) }> <span className="material-icons-outlined" style={{fontSize: '15px'}}> edit </span> </button>
                                <button className="btn btn-danger" onClick={() => props.deletar(produto.sku)}> <span className="material-icons-outlined" style={{fontSize: '15px'}}> delete </span> </button>
                            </td>
                        </tr>
                    )
                })
            }
        </tbody>
    </table>
)

export default ProdutosTable;
