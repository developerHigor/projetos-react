import React, { useEffect, useState} from "react";
import ProdutoService  from "../../app/produtoService";
import { useNavigate } from "react-router-dom";
import Card from "../../components/Card";
import ProdutosTable from "./produtosTable";

function ConsultaProdutos() {
    const service = new ProdutoService();
    const navigate = useNavigate();
    
    const [ produtos, setProdutos ] = useState([]);
    
    useEffect(() => {
        const prods = service.getProdutos();
        setProdutos( prods );
    }, []);

    function handlerEditar(sku){
        navigate(`/cadastro-produtos/${sku}`, { replace: false });
    }

    function handlerDeletar(sku){
        const objProdutos = service.deletar(sku);
        setProdutos(objProdutos);
    }
        
    return (
        <Card header="Consulta de Produto">
            <ProdutosTable produtos={produtos} 
                           editar={handlerEditar}
                           deletar={handlerDeletar} />
        </Card>
    )
}

export default ConsultaProdutos;
