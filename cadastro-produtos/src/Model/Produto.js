export default class Produto{
    constructor(obj){

        this.nome = obj != null ? obj.nome : '';
        this.sku = obj != null ? obj.sku : '';
        this.descricao = obj != null ? obj.descricao : '';
        this.fornecedor = obj != null ? obj.fornecedor : '';
        this.preco = obj != null ? obj.preco : 0;
    }
}
