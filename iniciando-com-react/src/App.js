import './App.css';
import React from 'react';

function App() {
  return (
    <div className="App">
      Hola
    </div>
  );
}

class ClassApp extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      nome: ""
    }
  }

  setInput = (event) => {
    const { value } = event.target;
    this.setState({
      nome: value
    })
  }

  render() {
    return(
      <>
        <input type="text" value={this.state.nome} onChange={this.setInput} />
        <h2>{this.state.nome}</h2>
      </>
    );
  }
}

export default ClassApp;
