import React, { useState } from 'react';


function App() {
  const [state, setState] = useState({
    numero1: 0,
    numero2: 0,
    resultado: 0
  });

  const somar = () =>{
    const numero1 = parseInt(state.numero1);
    const numero2 = parseInt(state.numero2);

    setState({...state, ...{resultado: numero1 + numero2}})
  }

  return (
    <div>
      Numero 1: <br />
      <input type="text" value={state.numero1} onChange={e => setState({...state, ...{numero1: e.target.value}})} />
      <br />

      Numero 2: <br />
      <input type="text" value={state.numero2} onChange={e => setState({...state, ...{numero2: e.target.value}})} />
      <br />

      <button onClick={somar}>
        Somar
      </button>
      Resultado: <br />
      <input type="text" value={state.resultado} onChange={e => setState({...state, ...{resultado: e.target.value}})} />
    </div>
  );
}

export default App;
