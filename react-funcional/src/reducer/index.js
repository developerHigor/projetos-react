import React, { useState } from 'react';
import useStore from './cacluloReducer';

function ReducerRook() {
    const [store, dispatch] = useStore();
    const [state, setState] = useState({
        numero1: 0,
        numero2: 0
    });


  const somar = () =>{
    const numero1 = parseInt(state.numero1);
    const numero2 = parseInt(state.numero2);

    dispatch({
        type: 'SOMA',
        payload: numero1 + numero2
    })
  }

  const subtrair = () =>{
    const numero1 = parseInt(state.numero1);
    const numero2 = parseInt(state.numero2);

    dispatch({
        type: 'SUBTRACAO',
        payload: numero1 - numero2
    })
  }

  return (
    <div>
      Numero 1: <br />
      <input type="text" value={state.numero1} onChange={e => setState({...state, ...{numero1: e.target.value}})} />
      <br />

      Numero 2: <br />
      <input type="text" value={state.numero2} onChange={e => setState({...state, ...{numero2: e.target.value}})} />
      <br />

      <button onClick={somar}> Somar </button>
      <button onClick={subtrair}> Subtrair </button>
      Resultado: <br />
      <input type="text" value={store.resultado} readOnly />
    </div>
  );
}

export default ReducerRook;
