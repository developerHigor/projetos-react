import { useReducer } from 'react';

const STATE_INITIAL = {
    resultado: 0
}

const calculoReducer = (state = STATE_INITIAL, action) => {
    switch (action.type) {
        case 'SOMA':
        case 'SUBTRACAO':
            return { ...state, resultado: action.payload }
        default:
            return state;
    }
}

const useStore = () => useReducer(calculoReducer, STATE_INITIAL);
export default useStore;
